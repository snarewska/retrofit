package com.exercises.retrofit.user;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/exercise/users")
@RestController
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

   //@GetMapping(value = "/tasks")
    @RequestMapping(value = "/tasks", method = RequestMethod.GET)
    public void displayUsersWithTasks(){
        userService.displayUsersWithTasks();
    }


}
