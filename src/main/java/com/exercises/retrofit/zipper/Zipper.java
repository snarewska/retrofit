package com.exercises.retrofit.zipper;

import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class Zipper {


    /**
     * pakuje pliki w paczki określonego rozmiaru, np.
     * plik 30 MB spakuje do
     * - part.zip
     * - part.z01
     * - part.z02
     * wszystkie po 10 MB
     * @throws ZipException
     */
    public void zip() throws ZipException {
        List<File> filesToAdd = Arrays.asList(
                new File("C:\\Users\\Enter\\Desktop\\kanca\\Zajęcia\\III rok\\check.jpg")
        );

        ZipFile zipFile = new ZipFile("C:\\Users\\Enter\\Desktop\\kanca\\Zajęcia\\III rok\\part.zip");
        zipFile.createSplitZipFile(filesToAdd, new ZipParameters(), true, 10485760);

        zipFile.getSplitZipFiles().forEach(s -> System.out.println(s.getName()));
    }
}
