package com.exercises.retrofit.user;

import com.exercises.retrofit.entity.User;
import retrofit2.Call;
import retrofit2.http.GET;

import java.util.List;

public interface UserApi {

    @GET("/users")
    Call<List<User>> getUsers();
}
