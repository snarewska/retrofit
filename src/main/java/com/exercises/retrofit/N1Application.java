package com.exercises.retrofit;

import com.exercises.retrofit.zipper.Zipper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

//@EnableScheduling
@SpringBootApplication
public class N1Application {

	public static void main(String[] args) {

		try {
			SpringApplication.run(N1Application.class, args);
			Zipper zipper = new Zipper();
			zipper.zip();
		} catch (Throwable e) {
			System.out.println(e.getLocalizedMessage());
			e.printStackTrace();
		}
	}

}
