package com.exercises.retrofit.todo;

import com.exercises.retrofit.entity.Todo;
import org.springframework.stereotype.Service;
import retrofit2.Retrofit;

import java.io.IOException;
import java.util.List;

@Service
public class TodoService {

    private final Retrofit retrofit;


    public TodoService(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    public List<Todo> getTodos() {
        TodoApi todoApi = retrofit.create(TodoApi.class);
        List<Todo> todos = null;
        try{
            todos = todoApi.getTodos().execute().body();
        }catch (IOException e) {
            throw new RuntimeException("During getting todos error occured");
        }
        return todos;

    }
}
