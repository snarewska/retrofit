package com.exercises.retrofit.user;

import com.exercises.retrofit.entity.Todo;
import com.exercises.retrofit.entity.User;
import com.exercises.retrofit.todo.TodoService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import retrofit2.Retrofit;

import java.io.IOException;
import java.util.List;

@Service
public class UserService {

    private final Retrofit retrofit;
    private final TodoService todoService;

    public UserService(Retrofit retrofit, TodoService todoService) {
        this.retrofit = retrofit;
        this.todoService = todoService;
    }


    public List<User> getUsers()  {
        UserApi userApi = retrofit.create(UserApi.class);
        List<User> users;
        try {
            users = userApi.getUsers().execute().body();
        } catch (IOException e) {
            throw new RuntimeException("During getting users error occured");
        }
        return users;
    }

//    @Scheduled(fixedDelay = 50000)
    public void displayUsersWithTasks(){
        List<User> users = getUsers();
        List<Todo> todos = todoService.getTodos();
        for (User user : users){
            System.out.println("User " + "#" + user.getId() + " (" + user.getName() + ")");
            for (Todo todo : todos){
                if(user.getId() == todo.getUserId())
                System.out.println("[*]" + "task:" + " " + todo.getTitle());
            }
        }



    }




}
