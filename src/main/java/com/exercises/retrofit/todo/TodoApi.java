package com.exercises.retrofit.todo;

import com.exercises.retrofit.entity.Todo;
import retrofit2.Call;
import retrofit2.http.GET;

import java.util.List;

public interface TodoApi {

    @GET("/todos")
    Call<List<Todo>> getTodos();
}
